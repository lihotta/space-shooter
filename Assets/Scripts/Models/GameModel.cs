using System;
using UnityEngine;

namespace SpaceShooter.Models
{
    public class GameModel
    {
        public int CurrentMaxScore { get; private set; }
        public int Score { get; private set; }
        public event Action ScoreChangedEvent;
        
        private const int INITIAL_SCORE = 0;
        private const int STAGE_CLEAR_SCORE = 10;
        private const int ENEMY_CLEAR_SCORE = 1;
        
        private const string MAX_SCORE = "MaxScore";

        public GameModel()
        {
            CurrentMaxScore = GetSaved(MAX_SCORE);
        }

        public void SaveMaxScore()
        {
            if (CurrentMaxScore > Score)
            {
                return;
            }

            CurrentMaxScore = Score;
            Save(MAX_SCORE, CurrentMaxScore);
        }

        public void InitScore()
        {
            Score = INITIAL_SCORE;
        }
        
        public void StageClear()
        {
            Score += STAGE_CLEAR_SCORE;

            if (ScoreChangedEvent != null)
            {
                ScoreChangedEvent();
            }
        }

        public void EnemyClear()
        {
            Score += ENEMY_CLEAR_SCORE;
            
            if (ScoreChangedEvent != null)
            {
                ScoreChangedEvent();
            }
        }
        
        
        private void Save(string key, int value)
        {
            PlayerPrefs.SetInt(key,value);
            PlayerPrefs.Save();
        }
        
        
        private int GetSaved(string key)
        {
            return PlayerPrefs.GetInt(key);
        }

    }
}