using System;
using SpaceShooter.Configs;

namespace SpaceShooter.Models
{
    public class SpaceShipModel
    {
        public float Life { get; private set; }

        public event Action LifeChangedEvent;
        public event Action DiedEvent;

        private const float MIN_LIFE = 0;
        private readonly SpaceShipConfig spaceShipConfig;
        
        public SpaceShipModel(SpaceShipConfig spaceShipConfig)
        {
            this.spaceShipConfig = spaceShipConfig;
        }

        public void Init()
        {
            Life = spaceShipConfig.Life;
            
            if (LifeChangedEvent != null)
            {
                LifeChangedEvent();
            }
        }

        public void TakeDamage()
        {
            Life--;
            
            if (LifeChangedEvent != null)
            {
                LifeChangedEvent();
            }

            if (Life <= MIN_LIFE && DiedEvent != null)
            {
                DiedEvent();
            }
        }

        public void RefillLife()
        {
            Life = spaceShipConfig.Life;
            if (LifeChangedEvent != null)
            {
                LifeChangedEvent();
            }
        }
    }
}