using System;
using SpaceShooter.Configs;
using SpaceShooter.Views;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceShooter.Models
{
    public class StageModel
    {
        public int TotalEnemiesInCurrentStage
        {
            get
            {
                if (squadPosition == null)
                {
                    Debug.LogWarning("There is no Stage initialized to get a total enemies.");
                    return -1;
                }

                return squadPosition.GetTotalPositions();
            }
        }

        public SpaceShipConfig EnemyShipConfig { get; private set; }
        public float FireDelayInCurrentStage { get; private set; }

        public event Action AllEnemiesDiedEvent;

        private readonly StageConfig[] stageConfigs;
        private int totalEnemiesDied;
        private int positionIndex;
        private int stageIndex;

        private SquadPosition squadPosition;
        private StageConfig selectedStageConfig;

        public StageModel(StageConfig[] stageConfigs)
        {
            this.stageConfigs = stageConfigs;
            stageIndex = 0;
        }

        public void SetNewStage()
        {
            SetNewStage(stageConfigs[stageIndex]);
            stageIndex = (stageIndex + 1) % stageConfigs.Length;        
        }
        
        public void SetNewStage(StageConfig stageConfig)
        {
            selectedStageConfig = stageConfig;
            squadPosition = selectedStageConfig.squadPosition;
            FireDelayInCurrentStage = selectedStageConfig.FireDelay;
            
            EnemyShipConfig = selectedStageConfig.enemyShipConfig;
            
            totalEnemiesDied = 0;
            positionIndex = 0;
   
        }

        public Vector3 GetNewPosition()
        {
            positionIndex = (positionIndex + 1) % TotalEnemiesInCurrentStage;
            return squadPosition.GetPositionAtIndex(positionIndex);
        }

        public void RemoveEnemy()
        {
            totalEnemiesDied++;
            if (totalEnemiesDied > 0 && totalEnemiesDied == TotalEnemiesInCurrentStage && AllEnemiesDiedEvent != null)
            {
                AllEnemiesDiedEvent();
            }
        }
    }
}