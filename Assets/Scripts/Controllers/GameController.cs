﻿using System;
using SpaceShooter.Views;
using SpaceShooter.Configs;
using SpaceShooter.Models;
using UnityEngine;

namespace SpaceShooter.Controllers
{
    public class GameController
    {
        public event Action ResetPlayerEvent;

        private PlayerShipController playerShipController;
        private StageController stageController;

        private readonly IGameUIView gameUiView;
        private float deltaPosition;

        private GameModel gameModel;

        private StageConfig initialStageConfig;
        private bool isTutorialOn;

        public GameController(IGameUIView gameUiView)
        {
            this.gameUiView = gameUiView;

            this.gameUiView.InputPressed += InputPressed;
            this.gameUiView.InputReleased += InputReleased;

            gameModel = new GameModel();
        }

        public void Shutdown()
        {
            gameUiView.InputPressed -= InputPressed;
            gameUiView.InputReleased -= InputReleased;
            gameModel.ScoreChangedEvent -= UpdateScore;
                        
            ShutdownStage();
            ShutdownPlayer();            
        }

        public void AddStage(StageBehaviour stageBehaviour, CollectibleBehaviour[] collectiblePrefabs,
            StageConfig[] stageConfigs, StageConfig initialStage)
        {
            this.initialStageConfig = initialStage;

            stageController = new StageController(stageBehaviour, collectiblePrefabs, stageConfigs);            

            gameModel.ScoreChangedEvent += UpdateScore;
            gameModel.InitScore();
            UpdateScore();
        }

        public void AddPlayer(SpaceShipBehaviour spaceShip, SpaceShipConfig spaceShipConfig)
        {
            var playerShipBehaviour = spaceShip.gameObject.GetComponent<PlayerShipBehaviour>();
            playerShipController = new PlayerShipController(spaceShip, playerShipBehaviour, spaceShipConfig);
        }

        public void StartTutorialMode()
        {
            gameUiView.UpdateMasScoreText(gameModel.CurrentMaxScore);
            
            isTutorialOn = true;
            gameUiView.EnableTutorialText(true);
            stageController.SetInitialStage(initialStageConfig);
            playerShipController.EnableDamage(false);
            
            InitPlayer();
            InitStage();
        }
               
        private void GameOver()
        {
            gameModel.SaveMaxScore();
            
            ShutdownStage();
            ShutdownPlayer();

            if (ResetPlayerEvent != null)
            {
                ResetPlayerEvent();
            }
            
            StartTutorialMode();
        }

        
        private void InitStage()
        {
            stageController.EnemyDiedEvent += ScoreEnemy;
            stageController.StageFinishedEvent += StageFinished;
            stageController.Init();
        }
        
        private void InitPlayer()
        {
            playerShipController.SpaceShipLifeChangedEvent += UpdateHealth;
            playerShipController.SpaceShipDiedEvent += GameOver;

            playerShipController.Init(MaskNames.Ally);
        }

        private void ShutdownPlayer()
        {
            playerShipController.SpaceShipLifeChangedEvent -= UpdateHealth;
            playerShipController.SpaceShipDiedEvent -= GameOver;
            playerShipController.Shutdown();
        }

        private void ShutdownStage()
        {
            stageController.EnemyDiedEvent -= ScoreEnemy;
            stageController.StageFinishedEvent -= StageFinished;
            stageController.Shutdown();
        }

        private void StageFinished()
        {
            gameModel.StageClear();

            if (isTutorialOn)
            {
                isTutorialOn = false;
                gameUiView.EnableTutorialText(false);
                playerShipController.EnableDamage(true);
                gameModel.InitScore();
            }
        }

        private void UpdateHealth(float life)
        {
            gameUiView.UpdateHealthText(life);
        }

        private void ScoreEnemy()
        {
            gameModel.EnemyClear();
        }

        private void UpdateScore()
        {
            gameUiView.UpdateScoreText(gameModel.Score);
        }

        private void InputPressed(InputType inputType)
        {
            switch (inputType)
            {
                case InputType.Left:
                    OnMoveLeft(true);
                    break;

                case InputType.Right:
                    OnMoveRight(true);
                    break;

                case InputType.Center:
                    OnShoot(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("inputType", inputType, null);
            }
        }

        private void InputReleased(InputType inputType)
        {
            switch (inputType)
            {
                case InputType.Left:
                    OnMoveLeft(false);
                    break;

                case InputType.Right:
                    OnMoveRight(false);
                    break;

                case InputType.Center:
                    OnShoot(false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("inputType", inputType, null);
            }
        }

        private void OnShoot(bool isPressed)
        {
            playerShipController.Shoot(isPressed);
        }

        private void OnMoveRight(bool isPressed)
        {
            playerShipController.MoveRight(isPressed);
        }

        private void OnMoveLeft(bool isPressed)
        {
            playerShipController.MoveLeft(isPressed);
        }
    }
}