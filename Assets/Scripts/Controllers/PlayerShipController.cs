using System;
using SpaceShooter.Views;
using SpaceShooter.Configs;

namespace SpaceShooter.Controllers
{
    public class PlayerShipController : SpaceShipControllerBase
    {
        private readonly SpaceShipConfig playerShipConfig;
        private readonly PlayerShipBehaviour playerSpaceShip;

        public PlayerShipController(SpaceShipBehaviour spaceShip, PlayerShipBehaviour playerSpaceShip, SpaceShipConfig playerShipConfig)
            : base(spaceShip, playerShipConfig)
        {
            this.playerSpaceShip = playerSpaceShip;
        }

        public new void Init(string spaceShipLayer)
        {
            base.Init(spaceShipLayer);
            spaceShip.CollectedItemEvent += ActivateCollectible;
        }

        public new void Shutdown()
        {
            base.Shutdown();
            spaceShip.CollectedItemEvent -= ActivateCollectible;
        }

        public void MoveLeft(bool isPressed)
        {
            if (!isPressed)
            {
                playerSpaceShip.MoveLeft(0);
                return;
            }

            playerSpaceShip.MoveLeft(speed);
        }

        public void MoveRight(bool isPressed)
        {
            if (!isPressed)
            {
                playerSpaceShip.MoveRight(0);
                return;
            }

            playerSpaceShip.MoveRight(speed);
        }

        public void Shoot(bool isPressed)
        {
            if (!isPressed)
            {
                spaceShip.StopFire();
                return;
            }

            spaceShip.StartFire();
        }
        
        public void EnableDamage(bool damageEnabled)
        {
            DamageEnabled = damageEnabled;
        }

        private void ActivateCollectible(CollectibleType collectibleType)
        {
            switch (collectibleType)
            {
                case CollectibleType.Health:
                    spaceShipModel.RefillLife();
                    break;
                case CollectibleType.GunBullet: 
                    spaceShip.SetSpaceShipGun(GunType.Bullet);
                    break;
                case CollectibleType.GunCircle:
                    spaceShip.SetSpaceShipGun(GunType.Circle);
                    break;
                case CollectibleType.GunDrop:
                    spaceShip.SetSpaceShipGun(GunType.Drop);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("collectibleType", collectibleType, null);
            }
        }
    }
}