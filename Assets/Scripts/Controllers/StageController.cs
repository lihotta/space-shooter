﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShooter.Views;
using SpaceShooter.Configs;
using SpaceShooter.Models;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceShooter.Controllers
{
    public class StageController
    {
        private StageBehaviour stageBehaviour;
        private List<EnemyShipController> enemyShipControllers;
        private CollectibleBehaviour[] collectiblePrefabs;

        private StageModel stageModel;

        public event Action EnemyDiedEvent;
        public event Action StageFinishedEvent;

        public StageController(StageBehaviour stageBehaviour, CollectibleBehaviour[] collectiblePrefabs, 
            StageConfig[] stageConfigs)
        {
            this.stageBehaviour = stageBehaviour;
            this.collectiblePrefabs = collectiblePrefabs;
            enemyShipControllers = new List<EnemyShipController>();

            stageModel = new StageModel(stageConfigs);
        }

        public void Init()
        {
            stageModel.AllEnemiesDiedEvent += EndCurrentStage;
        }

        public void Shutdown()
        {
            stageModel.AllEnemiesDiedEvent -= EndCurrentStage;
            ResetEnemyShipControllers();
        }

        public void SetInitialStage(StageConfig initialStage)
        {
            StartNewStage(initialStage);
        }
        
        private void ResetEnemyShipControllers()
        {
            foreach (var shipController in enemyShipControllers)
            {
                shipController.SpaceShipDiedEvent -= EnemyDie;
                shipController.Shutdown();
            }

            enemyShipControllers.Clear();
        }
        
        
        private void StartNewStage(StageConfig stageConfig)
        {
            stageModel.SetNewStage(stageConfig);
            StartLoadedStage();
        }
        
        private void StartRandomNewStage()
        {
            stageModel.SetNewStage();
            StartLoadedStage();
        }

        private void StartLoadedStage()
        {
            SpawnEnemies();

            InitSquadPosition();
            InitSquadFire();
        }
        private void SpawnEnemies()
        {
            var enemyShipPrefab = stageModel.EnemyShipConfig.ShipPrefab;
            stageBehaviour.SetNewSpaceShip(enemyShipPrefab);

            for (var i = 0; i < stageModel.TotalEnemiesInCurrentStage; i++)
            {
                var shipBehaviour = stageBehaviour.InstantiateEnemy();
                var enemyBehaviour = shipBehaviour.gameObject.GetComponent<EnemyShipBehaviour>();

                enemyShipControllers.Add(new EnemyShipController(shipBehaviour, enemyBehaviour, stageModel.EnemyShipConfig));
                enemyShipControllers[i].SpaceShipDiedEvent += EnemyDie;
                enemyShipControllers[i].Init(MaskNames.Enemy);
            }
        }

        private void InitSquadFire()
        {
            for (var i = 0; i < stageModel.TotalEnemiesInCurrentStage; i++)
            {
                enemyShipControllers[i].StartFire();
            }
        }

        private void InitSquadPosition()
        {
            for (var i = 0; i < stageModel.TotalEnemiesInCurrentStage; i++)
            {
                var position = stageModel.GetNewPosition();
                enemyShipControllers[i].StartMoving(position);
            }
        }

        private void EnemyDie()
        {
            stageModel.RemoveEnemy();

            if (EnemyDiedEvent != null)
            {
                EnemyDiedEvent();
            }
        }

        private void EndCurrentStage()
        {
            if (StageFinishedEvent != null)
            {
                StageFinishedEvent();
            }

            ResetEnemyShipControllers();
            
            CreateRandomCollectible();
            
            StartRandomNewStage();
            
        }

        private void CreateRandomCollectible()
        {
            var index = Random.Range(0, collectiblePrefabs.Length);
            var collectible = collectiblePrefabs[index];
            stageBehaviour.InstantiateCollectible(collectible);
        }
    }
}