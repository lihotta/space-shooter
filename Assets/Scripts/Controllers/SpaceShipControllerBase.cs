using System;
using SpaceShooter.Views;
using SpaceShooter.Configs;
using SpaceShooter.Models;

namespace SpaceShooter.Controllers
{
    public abstract class SpaceShipControllerBase
    {
        public event Action SpaceShipDiedEvent;
        public event Action<float> SpaceShipLifeChangedEvent;

        protected bool Alive { get; private set; }

        protected readonly float speed;
        protected readonly SpaceShipBehaviour spaceShip;
        protected readonly SpaceShipModel spaceShipModel;
        protected bool DamageEnabled;

        private readonly GunBehaviour[] shipGuns;

        protected SpaceShipControllerBase(SpaceShipBehaviour spaceShip,
            SpaceShipConfig spaceShipConfig)
        {
            this.spaceShip = spaceShip;
            speed = spaceShipConfig.Speed;
            shipGuns = spaceShipConfig.GunPrefab;

            spaceShipModel = new SpaceShipModel(spaceShipConfig);
        }

        public void Init(string spaceShipLayer)
        {
            spaceShip.ReceivedDamageEvent += ProcessDamage;

            spaceShip.SetSpaceShipLayer(spaceShipLayer);

            if (shipGuns.Length > 0)
            {
                var initialGun = shipGuns[0].Type;
                spaceShip.SetSpaceShipGun(initialGun);
            }

            spaceShipModel.LifeChangedEvent += LifeChanged;
            spaceShipModel.DiedEvent += SpaceShipDie;
            spaceShipModel.Init();
            Alive = true;
        }

        public void Shutdown()
        {
            spaceShip.ReceivedDamageEvent -= ProcessDamage;

            spaceShipModel.LifeChangedEvent -= LifeChanged;
            spaceShipModel.DiedEvent -= SpaceShipDie;

            if (spaceShip != null)
            {
                spaceShip.DestroyObject();
            }
        }

        private void LifeChanged()
        {
            if (SpaceShipLifeChangedEvent != null)
            {
                SpaceShipLifeChangedEvent(spaceShipModel.Life);
            }
        }

        private void SpaceShipDie()
        {
            if (SpaceShipDiedEvent != null)
            {
                SpaceShipDiedEvent();
            }

            Alive = false;
            spaceShip.ShowExplode();
            spaceShip.DestroyObject();
        }

        private void ProcessDamage()
        {
            spaceShip.ShowDamageEffects();

            if (!DamageEnabled)
            {
                return;
            }

            spaceShipModel.TakeDamage();
        }
    }
}