using System.Collections;
using SpaceShooter.Views;
using SpaceShooter.Configs;
using UnityEngine;

namespace SpaceShooter.Controllers
{
    public class EnemyShipController : SpaceShipControllerBase
    {
        private const float MIN_FIRE_DELAY = 1f;
        private const float MAX_FIRE_DELAY = 4f;

        private readonly EnemyShipBehaviour enemyShip;
        
        public EnemyShipController(SpaceShipBehaviour spaceShip, EnemyShipBehaviour enemyShip, 
            SpaceShipConfig spaceShipConfig) : base(spaceShip, spaceShipConfig)
        {
            this.enemyShip = enemyShip;
        }

        public new void Init(string spaceShipLayer)
        {
            base.Init(spaceShipLayer);
            DamageEnabled = true;
        }

        public void StartFire()
        {
            spaceShip.StartCoroutine(Fire());
        }
        private IEnumerator Fire()
        {
            var waitCondition = new WaitForSeconds(Random.Range(MIN_FIRE_DELAY, MAX_FIRE_DELAY));
            while (Alive)
            {
                if (!enemyShip.IsMoving)
                {
                    spaceShip.StartFire();
                }
                yield return waitCondition;
            }
        }

        public void StartMoving(Vector3 position)
        {
            enemyShip.MoveTo(position);
        }
    }
}