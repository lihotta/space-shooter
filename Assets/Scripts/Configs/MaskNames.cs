namespace SpaceShooter.Configs
{
    public static class MaskNames
    {
        public const string Enemy = "Enemy";
        public const string Ally = "Ally";
    }
}