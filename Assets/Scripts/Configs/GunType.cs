namespace SpaceShooter.Configs
{
    public enum GunType
    {
        Bullet,
        Circle,
        Drop
    }
}