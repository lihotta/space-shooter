﻿using SpaceShooter.Views;
using UnityEngine;

namespace SpaceShooter.Configs
{
	[CreateAssetMenu(menuName = "SpaceShooter/SpaceShip")]
	public class SpaceShipConfig : ScriptableObject
	{
		public GunBehaviour[] GunPrefab;
		public SpaceShipBehaviour ShipPrefab;
		public int Life;
		public float Speed;
	}	
}

