namespace SpaceShooter.Configs
{
    public enum CollectibleType
    {
        Health,
        GunCircle,
        GunDrop,
        GunBullet,
    }
}