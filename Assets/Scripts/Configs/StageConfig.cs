﻿using SpaceShooter.Views;
using UnityEngine;

namespace SpaceShooter.Configs
{
    [CreateAssetMenu(menuName = "SpaceShooter/StageConfig")]
    public class StageConfig : ScriptableObject
    {
        public SpaceShipConfig enemyShipConfig;
        public SquadPosition squadPosition;
        public float FireDelay;
    }
}