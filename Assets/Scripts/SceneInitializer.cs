﻿using System;
using SpaceShooter.Configs;
using SpaceShooter.Controllers;
using SpaceShooter.Views;
using UnityEngine;

namespace SpaceShooter.Initializer
{
    public class SceneInitializer : MonoBehaviour
    {
        [SerializeField] 
        private GameUIView gameUiView;

        [SerializeField] 
        private SpaceShipConfig spaceShipConfig;

        [SerializeField] 
        private StageBehaviour stageBehaviour;

        [SerializeField] 
        private StageConfig[] stageConfigs;
        
        [SerializeField] 
        private StageConfig initialStageConfig;

        [SerializeField] 
        private CollectibleBehaviour[] collectiblePrefabs;

        private GameController gameController;

        private void Awake()
        {
            gameController = new GameController(gameUiView);

            gameController.ResetPlayerEvent += AddPlayer;
            AddPlayer();
            
            gameController.AddStage(stageBehaviour, collectiblePrefabs, stageConfigs, initialStageConfig);
            gameController.StartTutorialMode();
        }

        private void AddPlayer()
        {
            var spaceShip = Instantiate(spaceShipConfig.ShipPrefab);
            gameController.AddPlayer(spaceShip, spaceShipConfig);
        }

        private void OnDestroy()
        {
            gameController.Shutdown();
        }
    }
}