﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShooter.Views
{
    public class SquadPosition : MonoBehaviour
    {
        public int GetTotalPositions()
        {
            return transform.childCount;
        }

        public Vector3 GetPositionAtIndex(int i)
        {
            return transform.GetChild(i).position;
        }
    }
}