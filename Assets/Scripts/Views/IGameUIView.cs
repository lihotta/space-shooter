﻿using System;
using SpaceShooter.Configs;

namespace SpaceShooter.Views
{
    public interface IGameUIView
    {
        event Action<InputType> InputPressed;
        event Action<InputType> InputReleased;
        void UpdateScoreText(int gameModelScore);
        void UpdateHealthText(float life);
        void EnableTutorialText(bool enabledText);
        void UpdateMasScoreText(int maxScore);
    }    
}
