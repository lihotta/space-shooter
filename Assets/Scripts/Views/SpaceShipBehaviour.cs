﻿using System;
using System.Collections.Generic;
using SpaceShooter.Configs;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace SpaceShooter.Views
{
    public class SpaceShipBehaviour : MonoBehaviour
    {
        public event Action ReceivedDamageEvent;
        public event Action<CollectibleType> CollectedItemEvent;
        
        private Dictionary<GunType, GunBehaviour> gunTypeToPrefab;
        
        [SerializeField]
        private GunBehaviour[] ShipGuns;
        
        [SerializeField]
        private ParticleSystem explosionPrefab;
        private ParticleSystem explosionFx;
        
        [SerializeField]
        private ParticleSystem damagePrefab;
        private ParticleSystem damageFx;

        private GunBehaviour spaceShipGun;
        private int spaceShipLayer;
        private const string COLLECTIBLE_TAG = "Collectible";

        public void SetSpaceShipLayer(string shipLayer)
        {
            spaceShipLayer = ~LayerMask.GetMask(shipLayer);
        }

        public void SetSpaceShipGun(GunType gunType)
        {
            if (gunTypeToPrefab.Count <= 0)
            {
                return;
            }

            if (spaceShipGun!= null)
            {
                Destroy(spaceShipGun.gameObject);
            }
            
            var gunPrefab = gunTypeToPrefab[gunType];
            spaceShipGun = Instantiate(gunPrefab, transform);
            spaceShipGun.SetCollisionLayer(spaceShipLayer);
        }

        public void StartFire()
        {
            if (spaceShipGun == null)
            {
                return;
            }

            spaceShipGun.EnableFire();
        }

        public void StopFire()
        {
            if (spaceShipGun == null)
            {
                return;
            }

            spaceShipGun.DisableFire();
        }

        public void ShowDamageEffects()
        {
            damageFx.transform.position = transform.position;
            damageFx.Play();
        }

        public void ShowExplode()
        {
            explosionFx.transform.position = transform.position;
            explosionFx.Play();
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }

        private void Awake()
        {
            gunTypeToPrefab = new Dictionary<GunType, GunBehaviour>();
            foreach (var gun in ShipGuns)
            {    
                if (gunTypeToPrefab.ContainsKey(gun.Type))
                {
                    Debug.LogWarning("SpaceShipBehaviour: Trying to add two shipGuns with same Type!");
                    return;
                }
                gunTypeToPrefab.Add(gun.Type, gun);
            }

            explosionFx = Instantiate(explosionPrefab);
            damageFx = Instantiate(damagePrefab, transform);
        }

        private void OnParticleCollision(GameObject other)
        {
            if (ReceivedDamageEvent != null)
            {
                ReceivedDamageEvent();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag(COLLECTIBLE_TAG) && CollectedItemEvent != null)
            {
                var collectibleItem = other.gameObject.GetComponent<CollectibleBehaviour>();
                CollectedItemEvent(collectibleItem.CollectibleType);
                collectibleItem.Shutdown();
            }
        }
    }
}