﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShooter.Configs;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SpaceShooter.Views
{
    public class InputButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        private InputType inputType;
        
        public event Action<InputType> Pressed;
        public event Action<InputType> Released;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            if (Pressed != null)
            {
                Pressed(inputType);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (Released != null)
            {
                Released(inputType);
            }
        }
    }
}