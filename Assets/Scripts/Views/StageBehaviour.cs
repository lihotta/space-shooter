﻿using UnityEngine;

namespace SpaceShooter.Views
{
    public class StageBehaviour : MonoBehaviour
    {
        private SpaceShipBehaviour spaceShip;

        private const float TOP_SPAWN_OFFSET = 10;
        private bool isMoving;

        public void SetNewSpaceShip(SpaceShipBehaviour spaceShipPrefab)
        {
            this.spaceShip = spaceShipPrefab;
        }

        public SpaceShipBehaviour InstantiateEnemy()
        {
            var shipObject = Instantiate(spaceShip, transform);

            var initialPosition = spaceShip.transform.position;
            initialPosition.y = initialPosition.y + TOP_SPAWN_OFFSET;
            shipObject.transform.position = initialPosition;
            
            return shipObject;
        }

        public void InstantiateCollectible(CollectibleBehaviour collectible)
        {
            Instantiate(collectible);
        }
    }
}