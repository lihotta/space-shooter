﻿using System;
using SpaceShooter.Configs;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace SpaceShooter.Views
{
    public class GameUIView : MonoBehaviour, IGameUIView
    {
        public event Action<InputType> InputPressed;
        public event Action<InputType> InputReleased;
        
        [SerializeField] 
        private InputButton leftButton;

        [SerializeField] 
        private InputButton rightButton;

        [SerializeField] 
        private InputButton centerButton;

        [SerializeField] 
        private Text scoreText;

        [SerializeField] 
        private Text healthText;

        [SerializeField] 
        private Text tutorialText;
        
        [SerializeField] 
        private Text maxScoreText;
  
        private const string SCORE_FORMAT = "SCORE: {0}";
        private const string HEALTH_FORMAT = "HEALTH: {0}";
        private const string MAX_SCORE_FORMAT = "MAX SCORE: {0}";
        
        public void UpdateScoreText(int score)
        {
            scoreText.text = string.Format(SCORE_FORMAT, score);
        }

        public void UpdateHealthText(float life)
        {
            healthText.text = string.Format(HEALTH_FORMAT, life);
        }
        
        public void EnableTutorialText(bool enabledText)
        {
            tutorialText.enabled = enabledText;
            maxScoreText.enabled = enabledText;
            
            healthText.enabled = !enabledText;
            scoreText.enabled = !enabledText;
        }

        public void UpdateMasScoreText(int maxScore)
        {
            var scoreText = "";
            if (maxScore > 0)
            {
                scoreText = string.Format(MAX_SCORE_FORMAT, maxScore);
            }

            maxScoreText.text = scoreText;
        }

        private void OnEnable()
        {
            leftButton.Pressed += OnInputPressed;
            leftButton.Released += OnInputReleased;

            rightButton.Pressed += OnInputPressed;
            rightButton.Released += OnInputReleased;

            centerButton.Pressed += OnInputPressed;
            centerButton.Released += OnInputReleased;
        }

        private void OnDisable()
        {
            leftButton.Pressed -= OnInputPressed;
            leftButton.Released -= OnInputReleased;

            rightButton.Pressed -= OnInputPressed;
            rightButton.Released -= OnInputReleased;

            centerButton.Pressed -= OnInputPressed;
            centerButton.Released -= OnInputReleased;
        }

        private void OnInputPressed(InputType inputType)
        {
            if (InputPressed != null)
            {
                InputPressed(inputType);
            }
        }

        private void OnInputReleased(InputType inputType)
        {
            if (InputReleased != null)
            {
                InputReleased(inputType);
            }
        }
    }
}