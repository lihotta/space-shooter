﻿using SpaceShooter.Configs;
using UnityEngine;
using UnityEngine.Serialization;

namespace SpaceShooter.Views
{
    public class GunBehaviour : MonoBehaviour
    {
        public GunType Type
        {
            get { return gunType; }
        }

        [SerializeField] 
        private ParticleSystem gunParticles;

        [SerializeField] 
        private GunType gunType;

        private bool IsLooping
        {
            get { return gunParticles.main.loop; }
        }

        private void Awake()
        {
            if (gunParticles == null)
            {
                gunParticles = GetComponent<ParticleSystem>();
            }
        }

        public void EnableFire()
        {
            gunParticles.Play();
        }

        public void DisableFire()
        {
            if (!IsLooping)
            {
                return;
            }

            gunParticles.Stop();
        }

        public void SetCollisionLayer(int damageLayer)
        {
            var gunCollision = gunParticles.collision;
            gunCollision.collidesWith = damageLayer;
        }

        public void SetLoop(bool isLooping)
        {
            var main = gunParticles.main;
            main.loop = isLooping;
        }
    }
}