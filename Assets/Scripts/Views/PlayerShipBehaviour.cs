﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShooter.Configs;
using SpaceShooter.Views;
using UnityEngine;

namespace SpaceShooter.Views
{
	[RequireComponent(typeof(SpriteRenderer), typeof(SpaceShipBehaviour))]
	public class PlayerShipBehaviour : MonoBehaviour
	{
		private Vector3 screenWidthInWorld;

		private float rightBoundary;
		private float leftBoundary;
		private float speed;
		private float speedLeft;
		private float speedRight;

		public void MoveRight(float speedX)
		{
			this.speedRight = speedX;
			StartMove();
		}

		public void MoveLeft(float speedX)
		{
			this.speedLeft = -speedX;
			StartMove();
		}
		
		private void StartMove()
		{
			speed = speedRight + speedLeft;
		}

		private void Awake()
		{
			screenWidthInWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f));
			var extentsX = gameObject.GetComponent<SpriteRenderer>().bounds.extents.x;
			rightBoundary = screenWidthInWorld.x - extentsX;
			leftBoundary = -screenWidthInWorld.x + extentsX;

		}

		private void Update()
		{
			if (Mathf.Abs(speed) <= 0)
			{
				return;
			}

			var deltaX = speed * Time.deltaTime;
			transform.Translate(deltaX, 0, 0);

			var newPosition = transform.position;
			newPosition.x = Mathf.Clamp(transform.position.x, leftBoundary, rightBoundary);
			transform.position = newPosition;
		}
	}
}