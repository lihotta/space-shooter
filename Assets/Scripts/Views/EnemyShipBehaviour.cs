﻿using System.Collections;
using System.Collections.Generic;
using SpaceShooter.Views;
using UnityEngine;

namespace SpaceShooter.Views
{
    [RequireComponent(typeof(SpriteRenderer), typeof(SpaceShipBehaviour))]
    public class EnemyShipBehaviour : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        private bool enableMoving;
        private Vector3 finalPosition;

        private const float DELTA_DISTANCE = 0.1f;
        public bool IsMoving
        {
            get
            {
                return Vector3.Distance(finalPosition, transform.position) > DELTA_DISTANCE;
            }
        }
        
        private void Update()
        {
            if (!enableMoving)
            {
                return;
            }

            transform.position = Vector3.MoveTowards(transform.position, finalPosition, speed * Time.deltaTime);
        }

        public void MoveTo(Vector3 position)
        {
            enableMoving = true;
            finalPosition = position;
        }
    }
}