﻿using System;
using SpaceShooter.Configs;
using UnityEngine;
using Random = UnityEngine.Random;

public class CollectibleBehaviour : MonoBehaviour
{

    public CollectibleType CollectibleType
    {
        get { return collectibleType; }
    }

    [SerializeField] 
    private CollectibleType collectibleType;
    
    [SerializeField] 
    private float speedY;

    private Vector3 screenWidthInWorld;
    private float topSpawn;
    private float bottomBoundary;
    private float rightBoundary;
    private float leftBoundary;
    private bool isMoving;

    public void Shutdown()
    {
        Destroy(gameObject);
    }

    private void Awake()
    {
        this.screenWidthInWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));

        var extentsY = gameObject.GetComponent<SpriteRenderer>().bounds.extents.y;
        var extentsX = gameObject.GetComponent<SpriteRenderer>().bounds.extents.x;

        this.bottomBoundary = -screenWidthInWorld.y - extentsY;
        this.leftBoundary = screenWidthInWorld.x - extentsX;
        this.rightBoundary = -screenWidthInWorld.x + extentsX;

        this.topSpawn = screenWidthInWorld.y + extentsY;
        
        SetRandomInitialPosition();
    }

    private void Start()
    {
        isMoving = true;
    }

    private void Update()
    {
        if (isMoving)
        {
            transform.Translate(0, -speedY * Time.deltaTime, 0);
            if (transform.position.y < bottomBoundary)
            {
                isMoving = false;
                Shutdown();
            }
        }
    }

    private void SetRandomInitialPosition()
    {
        var positionX = Random.Range(rightBoundary, leftBoundary);
        transform.position = new Vector3(positionX, topSpawn, 0);
    }
}