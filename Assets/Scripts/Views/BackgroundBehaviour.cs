﻿using UnityEngine;

namespace SpaceShooter.Views
{
    public class BackgroundBehaviour : MonoBehaviour
    {
        [SerializeField] private float speedY;

        private Renderer rend;
        private const string TEX_PROPERTY = "_MainTex";

        private void Start()
        {
            rend = GetComponent<Renderer>();
        }

        private void Update()
        {
            var offsetY = Time.time * speedY;

            rend.material.SetTextureOffset(TEX_PROPERTY, new Vector2(0, offsetY));
        }
    }
}