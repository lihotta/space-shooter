# Space Shooter!

A 2D Space ship shooter developed with Unity3D

## Getting Started

In this repository you can find: the Unity project and a APK

### Prerequisites

Developed in Unity3D **2017.4.12f1**. 
If imported in another version, make sure that are no missing links.

## Features

* Moving background
* Player can shoot and move left/right
* Different stages and enemies
* Enemies can shoot and kill the player
* 4 types of collectibles: 
    * Health Item: Refill player health
    * Circle Item: Set the player red-laser gun
    * Square Item: Set the player blue-laser gun
    * Triangle Item: Set the green-laser gun
* Damage and destroy visual effects
* Save the player max score
* Portrait orientation
* Endless game until the player dies

## Authors

* Livia Akemi Hotta (lahotta@gmail.com)
